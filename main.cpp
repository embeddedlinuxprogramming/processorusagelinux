//
// Created by jbgomezm on 10/27/23.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

int main() {
    ifstream file;
    double usage;
    file.open("/proc/stat");
    if (file.is_open()) {
        string line;
        if (getline(file, line)) {
            file.close();
            istringstream iss(line);
            int user, nice, system, idle, iowait, irq, softirq,
                steal, guest, guest_nice;
            string cpu;
            iss >> cpu >> user >> nice >> system >> idle >> iowait >> irq >>
                softirq >> steal >> guest >> guest_nice;
            double usage = 100.0 * ( 1 - double(idle)/double(user + nice +
                    system + idle + iowait + irq + softirq + steal + guest +
                    guest_nice));
            cout << "CPU usage: " << usage << "\%" << endl;
        }
        else {
            cout << "Error: couldn't read line from file." << endl;
            file.close();
        }
    }
    else cout << "Error: /proc/stat couldn't be openned." << endl;
    return 0;
}
